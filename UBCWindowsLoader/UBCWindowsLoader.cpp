// UBCWindowsLoader.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <windows.h>
#include <stdio.h>
#include <TlHelp32.h>
#include <fstream>

int main()
{
	STARTUPINFO si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	DEBUG_EVENT debugEvent = { 0 };
	int x;

	// Start process
	si.cb = sizeof(STARTUPINFO);

	char text[] = "SimpleConsole.exe";
	wchar_t wtext[20];
	size_t outSize;
	mbstowcs_s(&outSize, wtext, text, strlen(text) + 1);
	LPWSTR ptr = wtext;


	//if (!CreateProcess(0, ptr, 0, 0, TRUE, CREATE_SUSPENDED, 0, 0, &si, &pi))
	if (!CreateProcess(0, ptr, 0, 0, TRUE, 0, 0, 0, &si, &pi))
	{
		return 0x01;
	}
	printf("Created process %s : %d \n", "SimpleConsole.exe", pi.dwProcessId);

	// Debug the process
	/*DebugSetProcessKillOnExit(TRUE);
	DebugActiveProcess(pi.dwProcessId);*/

	CONTEXT* CTX;
	CTX = LPCONTEXT(VirtualAlloc(NULL, sizeof(CTX), MEM_COMMIT, PAGE_READWRITE));
	CTX->ContextFlags = CONTEXT_FULL; // Context is allocated
	if (GetThreadContext(pi.hThread, LPCONTEXT(CTX))) //if context is in thread
	{
		// Read instructions
		DWORD* ImageBase; //Base address of the image
		void* pImageBase; // Pointer to the image base
		printf("CTX->Ebx=%x\n", CTX->Ebx + 8);
		DWORD temp;
		unsigned long _numread;

		char buffer[1024];
		std::cout << "sizeof(buffer)=" << sizeof(buffer) << std::endl;
		ReadProcessMemory(pi.hProcess, (LPVOID)(CTX->Ebx + 8), buffer, sizeof(buffer), &_numread);
		std::cout << "_numread=" << _numread << std::endl;

		std::fstream* fs = new std::fstream("out.bin", std::ios::out | std::ios::binary);	
		fs->write(buffer, sizeof(buffer));
		fs->close();
		delete fs;

		//ReadProcessMemory(pi.hProcess, (LPVOID)(CTX->Ebx + 8), &temp, sizeof(temp), &_numread);
		//ReadProcessMemory(pi.hProcess, LPCVOID(CTX->Ebx + 8), LPVOID(&ImageBase), 4, 0);
		pImageBase = VirtualAllocEx(pi.hProcess, LPVOID(0), 100, 0x3000, PAGE_EXECUTE_READWRITE);
	}

	//do {
	//	if (!WaitForDebugEvent(&debugEvent, 10 * 1000 /*INFINITE*/))
	//	{
	//		// Timeout => go on error;
	//		goto FreezeProcessOnStartup_ERROR;
	//	}

	//	// Stop on debug exception
	//	switch (debugEvent.dwDebugEventCode)
	//	{
	//	case EXCEPTION_DEBUG_EVENT:
	//		goto FreezeProcessOnStartup_SUCCESS;

	//	case UNLOAD_DLL_DEBUG_EVENT:
	//		std::cout << "UNLOAD_DLL_DEBUG_EVENT" << std::endl;
	//		// Allow the x64 system dlls to be unloaded when dealing with a x86 process
	//		// To be really clean, we need to check if the currently unloaded dll is a 
	//		// well-known DLL. Unfortunately, there is no easy way to do so.
	//		/*
	//		if (!bTargetProcessArch)
	//		{
	//			goto FreezeProcessOnStartup_ERROR;
	//		}
	//		*/

	//	case LOAD_DLL_DEBUG_EVENT:
	//	case CREATE_THREAD_DEBUG_EVENT:
	//	case CREATE_PROCESS_DEBUG_EVENT:
	//		ContinueDebugEvent(
	//			debugEvent.dwProcessId,
	//			debugEvent.dwThreadId,
	//			DBG_CONTINUE);
	//		break;

	//	default:
	//		goto FreezeProcessOnStartup_ERROR;
	//	}

	//} while (true);


	// Clean process
	TerminateProcess(pi.hProcess, 0);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);


FreezeProcessOnStartup_ERROR:
	std::cout << "FreezeProcessOnStartup_ERROR" << std::endl;

	std::cin >> x;
	return false;

FreezeProcessOnStartup_SUCCESS:
	std::cout << "FreezeProcessOnStartup_SUCCESS" << std::endl;

	std::cin >> x;
	return true;

	std::cin >> x;
	return 0x00;
}
